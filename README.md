# Callanetica Italia

An unfinished project (due to customer cancellation) of a video lessons site with subscription plan.

This project was made by:
- [Giorgia Basso](https://gitlab.com/Giorget) (site design)
- [Matteo Benzi](https://gitlab.com/bnz_) (software dev)

