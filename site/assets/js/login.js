const BUTTON_LOG_REG = 'button-log-reg';
const GENERAL_ERROR = 'general-error';

const CSS_BTN_GREY = 'button-grey';
const CSS_BTN_STD = 'log-reg-btn';

const __lazy_elements = {};
function lazyElement(id) {
	let elem = __lazy_elements[id];
	if (!elem) {
		elem = document.getElementById(id);
		__lazy_elements[id] = elem;
	}
	return elem || console.error(`Element ${id} not found`);
}

function check_email(email) {
	return !!email.match(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?)*$/);
}

function check_passwd(passwd) {
	return passwd.length >= 8 && !!passwd.match(/[a-z]/) && !!passwd.match(/[A-Z]/) && !!passwd.match(/[0-9]/);
}

function checks(email, passwd) {
	let _checks = true;

	lazyElement(GENERAL_ERROR).parentElement.style.visibility = 'hidden';

	const _check_email = check_email(email);
	lazyElement('email-incorrect').style.visibility = !_check_email ? 'visible' : 'hidden';
	_checks &= _check_email;

	const _check_passwd = check_passwd(passwd);
	lazyElement('password-incorrect').style.visibility = !_check_passwd ? 'visible' : 'hidden';
	_checks &= _check_passwd;

	if(!_checks)
		return false;

	document.body.style.cursor = 'progress';
	lazyElement(BUTTON_LOG_REG).className = lazyElement(BUTTON_LOG_REG).className.replace(CSS_BTN_STD, CSS_BTN_GREY);

	return true;
}


function callApi(api, body, onsuccess) {
	fetch(api, {
		method: 'POST',
		body: JSON.stringify(body),
	}).then(async (resp) => {
		if(resp.ok) {
			onsuccess();
		} else {
			document.body.style.cursor = 'auto';
			lazyElement(BUTTON_LOG_REG).className = lazyElement(BUTTON_LOG_REG).className.replace(CSS_BTN_GREY, CSS_BTN_STD);
			lazyElement(GENERAL_ERROR).innerHTML = await resp.text();
			lazyElement(GENERAL_ERROR).parentElement.style.visibility = 'visible';
		}
	});
}

function signup() {
	const body = {
		email: lazyElement("email").value,
		passwd: lazyElement("password").value,
	};

	if(!checks(body.email, body.passwd))
		return;

	if(body.passwd === lazyElement('password-check').value) {
		lazyElement('password-not-match').style.visibility = 'hidden';
	} else {
		lazyElement('password-not-match').style.visibility = 'visible';
		document.body.style.cursor = 'auto';
		lazyElement(BUTTON_LOG_REG).className = lazyElement(BUTTON_LOG_REG).className.replace(CSS_BTN_GREY, CSS_BTN_STD);
		return;
	}

	callApi('/_api/user/signup', body, login);
}

function login() {
	const body = {
		email: lazyElement("email").value,
		passwd: lazyElement("password").value,
		remember_me: lazyElement('remember-me') ? lazyElement('remember-me').checked : undefined,
	};

	if(!checks(body.email, body.passwd))
		return;

	callApi('/_api/user/login', body, () => window.location.href = '/');
}
