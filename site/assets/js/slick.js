const arrow_button = `<button type='button' class='slick-{0} pull-left'><i class='button-slider-{0}' aria-hidden='true'></i></button>`;

function format(str, ...args) {
	for(let i=0; i<args.length; i++)
		str = str.replace(new RegExp(`\\{${i}\\}`, 'g'), args[i]);
	return str;
}

/* slider per i programmi in homepage */
$(document).ready(function () {
	$('#slide-program').slick({
		arrows: true,
		slidesToShow: 3,
		slideToScroll: 1,
		infinite: true,
		touchMove: true,
		autoplay: true,
		autoplaySpeed: 3000,
		prevArrow: format(arrow_button, "prev"),
		nextArrow: format(arrow_button, "next"),
		responsive: [
			{
				breakpoint: 992,
				settings: {
					slidesToShow: 2,
				}
			},
			{
				breakpoint: 767,
				settings: {
					slidesToShow: 1,
					arrows: false,
					dots: true
				}
			}
		]
	});
});

