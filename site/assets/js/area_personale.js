const ids = {
	'area_personale_nome': false,
	'area_personale_cognome': false,
	'area_personale_passwd': false,
};

function mod(id, button) {
	if(ids[id]) { //save
		button.innerHTML = "Modifica";
		document.getElementById(id).disabled = true;
		document.getElementById(id).className = "text_block";
	} else {
		button.innerHTML = "Salva";
		document.getElementById(id).disabled = false;
		document.getElementById(id).className = "";
	}
	ids[id] = !ids[id]; // turn off/on
}

function mod_passwd(button) {
	if(ids['area_personale_passwd']) {
		button.innerHTML = "Modifica";
		document.getElementById('passwd_hidden').style.display = 'none';
	} else {
		button.innerHTML = "Save";
		document.getElementById('passwd_hidden').style.display = 'block';
	}
	['area_personale_passwd', 'area_personale_new_passwd', 'area_personale_conf_passwd'].map(
		(id) => {
			document.getElementById(id).disabled = ids['area_personale_passwd'];
			document.getElementById(id).value = "";
		}
	);
	ids['area_personale_passwd'] = !ids['area_personale_passwd'];
}