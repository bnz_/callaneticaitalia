import 'dart:io' show Platform;
import 'package:mailer/mailer.dart';
import 'package:mailer/smtp_server.dart';
import 'constants.dart';
import 'utils.dart';
import 'database.dart';

/// a wrapper for the "mailer" package
class EmailSender {
	SmtpServer _server;
	final String email;
	final String accessToken;

	EmailSender():
		this.email = Platform.environment[ENV_EMAIL_NAME],
		this.accessToken = Platform.environment[ENV_EMAIL_TOKEN]
	{
		if (email == null || accessToken == null)
			throw Exception("Email name or access token not set correctly");
		_server = gmailSaslXoauth2(email, accessToken);
	}

	Future<bool> _sendMail(Message mail) async {
		try {
			await send(mail, _server);
			return true;
		} on MailerException catch(e) {
			log("Failing to send email: $e");
			return false;
		}
	}

	Future<bool> sendTo(String dest, String subject, String message) async {
		final mail = Message()
			..from = Address(email)
			..recipients.add(dest)
			..subject = subject
			..html = message;
		
		return await _sendMail(mail);
	}

	Future<bool> broadcast(String subject, String message) async {
		final mail = Message()
			..from = Address(email)
			..subject = subject
			..html = message;
		
		//TODO: remove this temporary "cheat"
		final users = database.select(TabUsers.table, [TabUsers.email], "1=1", []);
		if (users.length == 0)
			log("There is no user in the database", LogLvl.warning);
		for (var user in users)
			mail.recipients.add(user[TabUsers.email]);

		return await _sendMail(mail);
	}
}

//TODO: test it
final emailSender = EmailSender();
