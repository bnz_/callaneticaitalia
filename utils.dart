import 'dart:io';
import 'package:crypto/crypto.dart';
import 'dart:convert';
import 'package:collection/collection.dart';
import 'constants.dart';

/// hash the string, with optionally salt, using sha256
List<int> hash(String data, [String salt = '']) {
	return sha256.convert(utf8.encode(data + salt)).bytes;
}
/// hash the bytes, with optionally salt, using sha256
List<int> hash_bytes(List<int> data, [List<int> salt = const[]]) {
	return sha256.convert(data + salt).bytes;
}

/// compare 2 hashes (the length must be equal)
bool hash_eq(List<int> h1, List<int> h2) {
	int oks=0, i=0;
	while(i < h1.length) {
		oks += h1[i] == h2[i++] ? 1 : 0;
	}
	return oks == h1.length;
}

Function eq = const ListEquality().equals;

String httpReq2str(HttpRequest r) {
	return "${r.method} ${r.requestedUri}\n${r.headers}";
}

enum LogLvl { debug, log, warning, error }

/** Log the `msg` following the LogLvl behaviour
 * - `debug`: only write on stdout (in production this have no effect)
 * - `log`: write also into a file
 * - `warning`: put a "WARN" flag at the beginning of the `msg`
 * - `error`: put an "ERR" flag (in future this raise a notification to some bot)
 */
void log(String msg, [LogLvl lvl = LogLvl.log, HttpConnectionInfo connInfo]) async {
	String flag = lvl.index >= LogLvl.warning.index ? " (${lvl == LogLvl.warning? 'WARN' : 'ERR'})" : "";
	String ip = connInfo != null ? " (${connInfo.remoteAddress.address}:${connInfo.remotePort})" : "";
	msg = "${DateTime.now().toIso8601String()}$ip:$flag $msg";

	if(DEBUG) print(msg);
	if(lvl.index >= LogLvl.log.index) {
		File(LOG_FILE).writeAsStringSync(msg+'\n', mode: FileMode.append);
	}
}

/// shortcut for debug log only
void dbg(String msg) {
	log(msg, LogLvl.debug);
}

/// exception used to "send" messages trough the code flow
class MsgException implements Exception {
	final String msg;
	final int code;
	
	MsgException(this.msg, [this.code = 200]);

	@override
	String toString() {
		return msg;
	}
}

class Pair<T1, T2> {
	final T1 first;
	final T2 second;
	const Pair(this.first, this.second);
}
