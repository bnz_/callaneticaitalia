import 'dart:io';
import 'utils.dart';
import 'package:sqlite3/sqlite3.dart';
import 'constants.dart';

/// Name of table and columns for table Users
class TabUsers {
	static const String table = "Users";
	static const String email = "email";
	static const String passwd = "passwd";
	static const String subscribed = "subscribed";
	static const String name = "name";
	static const String surname = "surname";
}

/// Name of table and columns for table SessionCookies
class TabSessionCookies {
	static const String table = "SessionCookies";
	static const String value = "value";
	static const String user = "user";
	static const String creation = "creation";
}

/// Name of table and columns for table AuthCookies
class TabAuthCookies {
	static const String table = "AuthCookies";
	static const String selector = "selector";
	static const String token = "token";
	static const String user = "user";
	static const String expiration = "expiration";
}

/// Name of table and columns for table ResetTokens
class TabResetTokens {
	static const String table = "ResetTokens";
	static const String token = "token";
	static const String user = "user";
	static const String creation = "creation";
}

/// Name of table and columns for table Category
class TabCategory {
	static const String table = "Category";
	static const String name = "name";
}

/// Name of table and columns for table Videos
class TabVideos {
	static const String table = "Videos";
	static const String link = "link";
	static const String title = "title";
	static const String description = "description";
	static const String category = "category";
}


class DatabaseWrapper {
	final Database db;

	DatabaseWrapper([String db_location]) :
		db = db_location != null ? sqlite3.open(db_location) : sqlite3.openInMemory();


	/// read sql commands from a file
	bool read_sql(File sql) {
		try {
			db.execute(sql.readAsStringSync());
		} on SqliteException catch(e) {
			log("[DatabaseWrapper].read_sql(${sql?.path}): $e");
			return false;
		}
		return true;
	}

	bool insert(String table, List<String> columns, List<dynamic> values) {
		try {
			db.prepare("INSERT INTO $table(${columns.join(',')}) VALUES (${List<String>.generate(columns.length, (_)=>'?').join(',')})").execute(values);
		} on SqliteException catch(e) {
			log("[DatabaseWrapper].insert($table, $columns, $values): $e");
			return false;
		}
		return true;
	}

	bool update(String table, List<String> columns, List<dynamic> values, String where, List<dynamic> where_values) {
		try {
			db.prepare("UPDATE $table SET ${columns.map((c)=>'$c=?').join(',')} WHERE $where").execute(values + where_values);
		} on SqliteException catch(e) {
			log("[DatabaseWrapper].update($table, $columns, $values, $where, $where_values): $e");
			return false;
		}
		return true;
	}

	bool delete(String table, String where, List<dynamic> where_values) {
		try {
			db.prepare("DELETE FROM $table WHERE $where").execute(where_values);
		} on SqliteException catch(e) {
			log("[DatabaseWrapper].delete($table, $where, $where_values): $e");
			return false;
		}
		return true;
	}

	List<Map<String, dynamic>> select(String table, List<String> select, String where, List<dynamic> where_values) {
		try {
			ResultSet rs = db.select("SELECT ${select.join(',')} FROM $table WHERE $where", where_values);
			if(rs == null) return null;
			List<Map<String, dynamic>> result = [];
			for(final Row row in rs)
				result.add({}..addAll(row));
			return result;
		} on SqliteException catch(e) {
			log("[DatabaseWrapper].select($table, $select, $where, $where_values): $e");
			return null;
		}
	}

}

// standard database
final database = DatabaseWrapper(DB_LOCATION);

// in-memory database for session cookies
final database_sessions = DatabaseWrapper();
