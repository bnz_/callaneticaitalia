import 'dart:io';
import 'utils.dart';
import 'constants.dart';

const error_code_msg = {
	200 : "Ok",
	400 : "Bad request",
	401 : "Unauthorized",
	403 : "Forbidden",
	404 : "Not found",
	500 : "Internal server error",
};

const error_page_location = {
	200 : SITE_ROOT + "/assets/WiP.html",
	400 : SITE_ROOT + "/assets/WiP.html",
	401 : SITE_ROOT + "/assets/WiP.html",
	403 : SITE_ROOT + "/assets/WiP.html",
	404 : SITE_ROOT + "/assets/WiP.html",
	500 : SITE_ROOT + "/assets/WiP.html",
};

/// respond with the error code.
/// If the request is a GET it send back the correspondent page,
/// otherwise a plain message
void errorPage(int code, HttpRequest r, [String message]) async {
	try {
		r.response.statusCode = code;
		if(r.method == 'GET') {
			r.response.headers.contentType = ContentType.html;
			await r.response.addStream(
				File(error_page_location[code] ?? "$SITE_ROOT/assets/WiP.html").openRead()
			);
		} else {
			r.response.write("$code, " + (error_code_msg[code] ?? "Unexpected error code"));
			if(message != null)
				r.response.write("\n"+message);
		}
		await r.response.close();
		log("Request ${r.uri} cause a ${r.response.statusCode} error", LogLvl.log, r.connectionInfo);
	} catch (e, s) {
		log("INCEPTION $e (stacktrace: $s)", LogLvl.error, r.connectionInfo);
	}
}


/// classic error-page case wrapped into functions
class ErrorPageCase {

	static void badApiRequest(HttpRequest r, [Map body]) {
		errorPage(400, r);
		log("Bad _api request: ${httpReq2str(r)}" + (body?.toString() ?? ''), LogLvl.warning, r.connectionInfo);
	}

}
