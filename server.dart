import 'dart:async';
import 'dart:ffi';
import 'dart:io';
import 'assets.dart';
import 'constants.dart';
import 'error_page.dart';
import 'database.dart';
import 'package:sqlite3/open.dart';
import 'utils.dart';
import 'users.dart';

Future main(List<String> args) async {

	/************ init system ************/
	print("Initializing system...");

	if(Platform.environment[ENV_DEBUG_FLAG] != null)
		DEBUG = false; // production mode

	open.overrideFor(OperatingSystem.linux, _openOnLinux); // Just be sure to first override the behavior and then use sqlite3
	if(!database.read_sql(File(DB_INIT_SQL)) || !database_sessions.read_sql(File(DB_SESSION_COOKIES_INIT_SQL))) { // init the databases
		print("An error occurred. Abort");
		return;
	}


	// listener trigger eTag re-calc //
	await calcETags();

	File(ETAG_TRIGGER).watch(events: FileSystemEvent.modify).forEach((element) {
		if((element as FileSystemModifyEvent).contentChanged) {
			calcETags();
			log("[ETAG] triggered eTag recalculate");
		}
	}).catchError((e,s) {
		log("[ETAG] error while triggered $e (stacktrace: $s)", LogLvl.warning);
	});

	// scheduled jobs //
	Timer.periodic(new Duration(hours: 6), (Timer t) {
		const tag = "[cleanup]";
		log("$tag: started");

		// delete expired session cookies (1d)
		if(!database_sessions.delete(TabSessionCookies.table, "${TabSessionCookies.creation} < datetime('now','-1 day')", []))
			log("$tag: Unable to delete expired sessions cookies", LogLvl.warning);

		// delete expired auth cookies (30d)
		if(!database.delete(TabAuthCookies.table, "${TabAuthCookies.expiration} < ?", [new DateTime.now().millisecondsSinceEpoch]))
			log("$tag: Unable to delete expired auth cookies", LogLvl.warning);

		// delete expired reset tokens (1h)
		if(!database.delete(TabResetTokens.table, "${TabResetTokens.creation} < datetime('now','-1 hour')", []))
			log("$tag: Unable to delete expired reset tokens", LogLvl.warning);

		log("$tag: ended");
	});

	/************ init server ************/
	print("Initializing server...");
	var server = await HttpServer.bind(
		InternetAddress.anyIPv4,
		args.length > 0 ? int.parse(args[0]) : 8080,
	);
	print('Listening on http://127.0.0.1:${server.port}');

	await for (HttpRequest request in server) {
		handleRequest(request).catchError((e,s) {
			errorPage(500, request);
			log("An error occurred: $e (stacktrace: $s)", LogLvl.error);
		});
	}
}

Future handleRequest(HttpRequest request) async {
	/* try to auto-login with auth cookie */
	AuthCookie auth;
	if(!SessionCookie.fromCookies(request.cookies).isValid() && (auth = AuthCookie.fromCookies(request.cookies)).isValid()) {
		dbg("Auth detect: " + request.uri.toString());
		AuthCookie.delete(cookie: auth.cookie);
		final cookies = makeCookie4user(auth.user, auth:true);
		request.response
			..cookies.add(cookies.first)
			..cookies.add(cookies.second)
			..redirect(request.uri)
			..close();
		return;
	}
	auth = null; //release the resource

	dbg("request uri: " + request.uri.toString());
	if(request.uri.path == "/" || request.uri.path.toLowerCase() == "/index.html") {
		await handleAssets("/index.html", request);
	} else if(request.uri.path.startsWith(API_ROOT)) {
		// _api //
		if(request.uri.path.startsWith(API_USER_ROOT)) {
			await handleUserApi(request);
		} else if(request.uri.path.startsWith(API_USER_PING)) {
			await request.response
				..write("pong")
				..close();
		} else {
			ErrorPageCase.badApiRequest(request);
		}
	} else { //try to send the correspondent asset
		await handleAssets(request.uri.path, request);
	}
}

DynamicLibrary _openOnLinux() {
	final script = File(Platform.script.resolve("./data").path);
	dbg("script path: ${script.path}");
	return DynamicLibrary.open(File('${script.path}/sqlite3.so').path);
}
