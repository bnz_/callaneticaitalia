import 'dart:io';
import 'dart:math';
import 'dart:convert';
import 'constants.dart';
import 'utils.dart';
import 'database.dart';
import 'error_page.dart';

final emailValidator = new RegExp(r"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?)*$");

/// simple password validation (at least 9 char, one letter, capital letter and a number)
final passwdValidator = (String passwd) => passwd.length > 8 && passwd.contains(RegExp(r"[A-Z]")) && passwd.contains(RegExp(r"[a-z]")) && passwd.contains(RegExp(r"[0-9]"));

abstract class AbsCookieManager {
	final Cookie cookie;
	String _user = null;

	String get user {
		if(_user == null && cookie != null) {
			_user = _getAssociatedUser() ?? "";
		}
		return _user == "" ? null : _user;
	}

	AbsCookieManager(this.cookie);

	String _getAssociatedUser();

	bool isValid() {
		return user != null;
	}

	@override
	String toString() {
		return "${cookie} (user: ${user})";
	}
}

class SessionCookie extends AbsCookieManager {

	SessionCookie(Cookie cookie) : super(cookie);
	SessionCookie.fromCookies(List<Cookie> cookies)
		: super(cookies.firstWhere((c) => c.name == SESSION_COOKIE, orElse: () => null));

	String _getAssociatedUser() {
		final dbr = database_sessions.select(TabSessionCookies.table, [TabSessionCookies.user], "${TabSessionCookies.value}=?", [cookie.value]);
		return dbr!=null && dbr.length>0 ? dbr[0][TabSessionCookies.user] : null;
	}

	static Cookie generate() {
		final rnd = Random.secure();
		final value = base64Encode(List<int>.generate(16, (_) => rnd.nextInt(255)));
		return new Cookie(SESSION_COOKIE, value)
			..httpOnly = true;
			//TODO: why not SameSite=Strict?
	}

	static bool save(Cookie cookie, String user) {
		return database_sessions.insert(TabSessionCookies.table,
			[TabSessionCookies.value, TabSessionCookies.user],
			[cookie.value, user]
		);
	}

	static bool delete({Cookie cookie, String user}) {
		final column = cookie==null ? TabSessionCookies.user : TabSessionCookies.value;
		final value = cookie==null ? user : cookie.value;
		return database_sessions.delete(TabSessionCookies.table, "$column=?", [value]);
	}
}

class AuthCookie extends AbsCookieManager {
	static const _selector_length = 16;
	static const _token_length = 32;

	AuthCookie(Cookie cookie) : super(cookie);
	AuthCookie.fromCookies(List<Cookie> cookies)
		: super(cookies.firstWhere((c) => c.name == AUTH_COOKIE, orElse: () => null));
	
	String _getAssociatedUser() {
		final unpacked = unpack(cookie);
		final dbr = database.select(TabAuthCookies.table,
			[TabAuthCookies.token, TabAuthCookies.user, TabAuthCookies.expiration],
			"${TabAuthCookies.selector}=?",
			[unpacked.first]
		);
		if(
			dbr == null || dbr.length < 1 ||
			!hash_eq(hash_bytes(unpacked.second), dbr[0][TabAuthCookies.token]) ||
			DateTime.fromMillisecondsSinceEpoch(dbr[0][TabAuthCookies.expiration]).isBefore(DateTime.now())
		) return null;
		return dbr[0][TabAuthCookies.user];
	}

	static Pair<List<int>,List<int>> unpack(Cookie cookie) {
		final bytes = base64Decode(cookie.value);
		return Pair(bytes.sublist(0,_selector_length), bytes.sublist(_selector_length+1));
	}

	static Cookie generate() {
		final rnd = Random.secure();
		final selector = List<int>.generate(_selector_length, (_) => rnd.nextInt(255));
		final token = List<int>.generate(_token_length, (_) => rnd.nextInt(255));
		return new Cookie(AUTH_COOKIE, base64Encode(selector+token))
			..expires = (new DateTime.now().add(Duration(days: 30)))
			..httpOnly = true;
			//TODO: why not SameSite=Strict?
	}

	static bool save(Cookie cookie, String user) {
		final unpacked = unpack(cookie);
		return database.insert(TabAuthCookies.table,
			[TabAuthCookies.selector, TabAuthCookies.token, TabAuthCookies.user, TabAuthCookies.expiration],
			[unpacked.first, hash_bytes(unpacked.second), user, cookie.expires.millisecondsSinceEpoch]
		);
	}

	static bool delete({Cookie cookie, String user}) {
		final column = cookie==null ? TabAuthCookies.user : TabAuthCookies.selector;
		final value = cookie==null ? user : unpack(cookie).first;
		return database.delete(TabAuthCookies.table, "$column=?", [value]);
	}
}

/// generate and save a new session (an optionally an auth) cookie \
/// Return: `Pair(session, auth)`
/// > NB: the deleting process is demand to the caller
Pair<Cookie, Cookie> makeCookie4user(String user, {bool auth = false}) {
	final session = SessionCookie.generate();
	SessionCookie.save(session, user);
	if(auth) {
		final auth_cookie = AuthCookie.generate();
		AuthCookie.save(auth_cookie, user);
		return Pair(session, auth_cookie);
	}
	return Pair(session, null);
}

/// check if the password associated to the email is inside the database
/// if the passwd is valid returns true
bool check_passwd(String email, String passwd_plain) {
	return database.select(
		TabUsers.table,
		['0'], //random thing to count the number of result
		"${TabUsers.email}=? AND ${TabUsers.passwd}=?",
		[email, hash(passwd_plain, email)]
	).length == 1;
}

Future<Map> extractBody(HttpRequest request) async {
	try {
		return jsonDecode(await utf8.decoder.bind(request).join()) as Map;
	} catch(e) {
		log("[extractBody]: $e", LogLvl.warning, request.connectionInfo);
		return {};
	}
}


void handleUserApi(HttpRequest request) {
	if(request.uri.path.startsWith(API_USER_SIGNUP) && request.method == 'POST') {
		signup(request);
	} else if(request.uri.path.startsWith(API_USER_LOGIN) && request.method == 'POST') {
		login(request);
	} else if(request.uri.path.startsWith(API_USER_LOGIN) && request.method == 'GET') {
		login_get(request);
	} else if(request.uri.path.startsWith(API_USER_LOGOUT)) {
		logout(request);
	} else if(request.uri.path.startsWith(API_USER_INFO)) {
		info(request);
	} else if(request.uri.path.startsWith(API_USER_MOD) && request.method == 'POST') {
		modify_user_data(request);
	} else if(request.uri.path.startsWith(API_USER_DELETE) && request.method == 'POST') {
		delete(request);
	} else if(request.uri.path.startsWith(API_USER_ASK_RESET) && request.method == 'POST') {
		requestResetToken(request);
	} else if(request.uri.path.startsWith(API_USER_RESET) && request.method == 'POST') {
		verifyResetToken(request);
	} else {
		ErrorPageCase.badApiRequest(request);
	}
}

/// signup via email + password (+ name + username)
/// > if the request contains a valid session cookie the request will be dropped
void signup(HttpRequest request) async {
	final session = SessionCookie.fromCookies(request.cookies);
	var resp = "";

	if(!session.isValid()) try {
		final body = await extractBody(request);
		final sanitizer = new HtmlEscape();
		final String email = body[FORM_EMAIL] ?? (throw MsgException("Email vuota", 400));
		final String passwd = body[FORM_PASSWD] ?? (throw MsgException("Password vuota", 400));
		final String name = sanitizer.convert(body[FORM_NAME] ?? "");
		final String surname = sanitizer.convert(body[FORM_SURNAME] ?? "");

		// email validation
		if(!emailValidator.hasMatch(email))
			throw MsgException("Email non valida", 400);
		
		if(!passwdValidator(passwd))
			throw MsgException("Password non valida", 400);

		if(!database.insert(TabUsers.table,
			[TabUsers.email,TabUsers.passwd,TabUsers.name,TabUsers.surname],
			[email, hash(passwd, email), name, surname]
		)) throw MsgException("L'utente esiste giá", 403);
		
		//TODO: send a welcome email

	} on MsgException catch(e) {
		request.response.statusCode = e.code;
		resp = e.toString();
	}

	//NB: in this case the redirect is managed by js
	// an 200 empty resp means that the user il logged
	request.response
		..write(resp)
		..close();
}

void modify_user_data(HttpRequest request) async {
	final session = SessionCookie.fromCookies(request.cookies);
	var resp = "";

	if(session.isValid()) try {
		final body = await extractBody(request);
		final sanitizer = new HtmlEscape();
		final List<String> columns = [];
		final List<dynamic> values = [];

		if((body[FORM_NEW_PASSWD] != null)) {
			if(!passwdValidator(body[FORM_NEW_PASSWD]))
				throw MsgException("Password non valida", 400);
			if(!check_passwd(session.user, body[FORM_PASSWD] ?? (throw MsgException("Devi inserire anche la password corrente", 400))))
				throw MsgException("Devi inserire anche la password corrente", 400);
			columns.add(TabUsers.passwd);
			values.add(hash(body[FORM_NEW_PASSWD], session.user));
		}
		if(body[FORM_NAME] != null) {
			columns.add(TabUsers.name);
			values.add(sanitizer.convert(body[FORM_NAME]));
		}
		if(body[FORM_SURNAME] != null) {
			columns.add(TabUsers.surname);
			values.add(sanitizer.convert(body[FORM_SURNAME]));
		}

		if(columns.length > 0) {
			if(!database.update(TabUsers.table,
				columns,
				values,
				"${TabUsers.email}=?",
				[session.user]
			)) {
				errorPage(500, request);
				log("Unable to update user ${session.user}, ${httpReq2str(request)} - $body", LogLvl.error, request.connectionInfo);
				return;
			}
		}

	} on MsgException catch(e) {
		request.response.statusCode = e.code;
		resp = e.toString();
	} else {
		errorPage(401, request);
		return;
	}

	request.response
		..write(resp)
		..close();
}

/// login via email + password
void login(HttpRequest request) async {
	var resp = "";

	try {
		final body = await extractBody(request);
		final String email = body[FORM_EMAIL] ?? (throw MsgException("Email vuota", 400));
		final String passwd = body[FORM_PASSWD] ?? (throw MsgException("Password vuota", 400));

		final user = database.select(TabUsers.table,
			[TabUsers.email, TabUsers.passwd],
			"${TabUsers.email}=?",
			[email]
		) ?? (throw MsgException("Email o password sbagliata", 403));

		if(user.length > 0 && hash_eq(hash(passwd, email), user[0][TabUsers.passwd])) {
			AuthCookie.delete(user: email);
			final cookies = makeCookie4user(email, auth: body[FORM_REMEMBER] ?? false);
			request.response.cookies.add(cookies.first);
			if(cookies.second != null)
				request.response.cookies.add(cookies.second);
		}
		else throw MsgException("Email o password sbagliata", 403);
	} on MsgException catch(e) {
		request.response.statusCode = e.code;
		resp = e.toString();
	}

	//NB: in this case the redirect is managed by js
	// a 200 empty resp means that the user il logged
	request.response
		..write(resp)
		..close();
}

/// login via GET which use the auth cookie to login
/// 
/// this is needed because, for performance reason, the "auto-login" mechanism didn't check
/// if the session cookie is invalid
void login_get(HttpRequest request) async {
	if(SessionCookie.fromCookies(request.cookies).isValid()) {
		request.response
			..redirect(Uri(path: '/'))
			..close();
		return;
	}

	final auth_cookie = AuthCookie.fromCookies(request.cookies);
	if(auth_cookie.isValid()) {
		AuthCookie.delete(cookie: auth_cookie.cookie);
		final cookies = makeCookie4user(auth_cookie.user, auth:true);

		request.response
			..cookies.add(cookies.first)
			..cookies.add(cookies.second)
			..redirect(Uri(path: '/'))
			..close();
	}
	else {
		request.response
			..redirect(Uri(path: '/assets/WiP.html')) //TODO: path to the login page
			..close();
	}
}

void logout(HttpRequest request) async {
	final session = SessionCookie.fromCookies(request.cookies);
	if(session.user != null) {
		if(!SessionCookie.delete(user: session.user)) {
			log("Failed to delete all session cookie of ${session.user}");
		}
		if(!AuthCookie.delete(user: session.user)) {
			log("Failed to delete all auth cookie of ${session.user}");
		}
	}

	request.response
		..redirect(Uri(path:'/'))
		..close();
}

/// return user info (if it's allowed to do that) in json format
/// > {email, name, username, ...}
void info(HttpRequest request) async {
	final session = SessionCookie.fromCookies(request.cookies);
	Map<String, String> response = {};

	if(session.isValid()) try {
		final dbr = database.select(TabUsers.table,
			[TabUsers.name, TabUsers.surname],
			"${TabUsers.email}=?",
			[session.user]
		);
		if(dbr == null || dbr.length == 0)
			throw MsgException("User ${session.user} not found", 500);

		response = dbr[0].cast<String, String>();
		response[TabUsers.email] = session.user;

	} on MsgException catch(e) {
		request.response.statusCode = e.code;
		log(e.toString(), LogLvl.error, request.connectionInfo);
	} else {
		errorPage(401, request);
		return;
	}

	request.response
		..headers.contentType = ContentType.json
		..write(jsonEncode(response))
		..close();
}

void delete(HttpRequest request) async {
	final session = SessionCookie.fromCookies(request.cookies);

	if(session.isValid()) try {
		final body = await extractBody(request);

		if(!check_passwd(session.user, body[FORM_PASSWD] ?? (throw MsgException("Password vuota", 400))))
			throw MsgException("Password sbagliata", 403);

		// delete the account
		if(!database.delete(TabUsers.table,
			"${TabUsers.email}=?",
			[session.user])
		) {
			errorPage(500, request);
			log("Unable to delete the user ${session.user}, ${httpReq2str(request)}", LogLvl.error, request.connectionInfo);
			return;
		}

		//delete the associated cookies
		SessionCookie.delete(user: session.user);
		AuthCookie.delete(user: session.user);

		request.response
			..redirect(Uri(path:"/assets/WiP.html")) //TODO
			..close();
	} on MsgException catch(e) {
		errorPage(e.code, request, e.toString());
	} else {
		errorPage(401, request);
	}
}

/// provides a token for reset the user's password
String genResetToken(String user) {
	final rnd = Random.secure();
	final token = List<int>.generate(32, (_) => rnd.nextInt(255));
	if(!database.insert(TabResetTokens.table,
		[TabResetTokens.user, TabResetTokens.token, TabResetTokens.creation],
		[user, hash_bytes(token), DateTime.now().millisecondsSinceEpoch]
	)) {
		log("Unable to insert the reset token for the user $user ($token)", LogLvl.error);
		return null;
	}
	return base64Encode(token);
}


/// returns the email of the user if the token is valid, otherwise null
String getUserFromResetToken(String token) {
	int creation;
	String user;
	try {
		final res = database.select(TabResetTokens.table,
			[TabResetTokens.user, TabResetTokens.creation],
			"${TabResetTokens.token}=?",
			[hash_bytes(base64Decode(token).toList())]
		)[0];
		creation = res[TabResetTokens.creation];
		user = res[TabResetTokens.user];
	} on FormatException catch (e) {
		log("Unable to decode reset token: $e", LogLvl.warning);
		return null;
	} on RangeError {
		log("Token $token not found", LogLvl.warning);
		return null;
	}
	// is created less than 1 hour ago?
	return DateTime.fromMillisecondsSinceEpoch(creation).isAfter(DateTime.now().subtract(Duration(hours: 1)))
		? user
		: null;
}

// reset password flow:
// 	user X request a reset token [POST]{email}--> a new token will be generated for that user and an email will be sent
// 	user X open the link of the email [GET]{token}--> insert the new password [POST]{token,password}--> if the token is valid the password for that user will be changed

/// handle the request for a new reset token: verify if the mail exists, generate a new token and send an email with the token
void requestResetToken(HttpRequest request) async {
	final body = await extractBody(request);
	String email;
	try {
		email = body[FORM_EMAIL] ?? (throw MsgException("Missing email"));

		if(database.select(TabUsers.table,
			[TabUsers.email],
			"${TabUsers.email}=?",
			[email]
		).length == 1) {
			final token = genResetToken(email);
			dbg("reset toekn: $token");

			if(token != null) {
				//TODO: send email
				dbg("TODO: send email with \"$token\"");
			} else {
				errorPage(500, request);
			}
		} else {
			log("[requestResetToken]: user '$email' not found");
		}
	} on MsgException catch (e) {
		errorPage(400, request, e.toString());
		return;
	}
	request.response.close(); //TODO: give the possibility to guess which user exists?
}

/// verifies the token validity and eventually accept the new password for the user
void verifyResetToken(HttpRequest request) async {
	final String rtoken = request.uri.queryParameters[QUERY_RTOKEN]?.replaceAll(' ', '+'); //TODO: handle better the decoding of base64
	if(rtoken != null) {
		final user = getUserFromResetToken(rtoken);
		if(user != null) {
			final new_passwd = (await extractBody(request))[FORM_PASSWD];
			if(new_passwd != null && passwdValidator(new_passwd)) {
				if(!database.update(TabUsers.table,
					[TabUsers.passwd],
					[hash(new_passwd, user)],
					"${TabUsers.email}=?",
					[user]
				)) {
					errorPage(500, request);
					log("[verifyResetToken] Unable to update user $user", LogLvl.error);
					return;
				}
				if(!database.delete(TabResetTokens.table,
					"${TabResetTokens.user}=?",
					[user]
				)) {
					log("[verifyResetToken] Unable to delete all the reset tokens for the user $user", LogLvl.error);
				}
				request.response.write("Password resettata con successo");
			} else {
				request.response.statusCode = 400;
				request.response.write("Password non valida");
			}
		} else {
			request.response.statusCode = 400;
			request.response.write("Link scaduto");
		}
	} else {
		request.response.statusCode = 400;
		request.response.write("Bad request");
	}
	request.response.close();
}

