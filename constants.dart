/* environment */
bool DEBUG = true;
const ENV_DEBUG_FLAG = "CALLANETICA_PRODUCTION";
const ENV_EMAIL_NAME = "CALLANETICA_EMAIL";
const ENV_EMAIL_TOKEN = "CALLANETICA_EMAIL_TOKEN";

/* roots site */
const SITE_ROOT = "site/";
const ETAG_TRIGGER = "${SITE_ROOT}etag_trigger"; //file to modify (touch) to trigger the recalculation of the eTags for the assets
const API_ROOT = "/_api";
	const API_USER_ROOT = "$API_ROOT/user";
		const API_USER_SIGNUP = "$API_USER_ROOT/signup";
		const API_USER_LOGIN = "$API_USER_ROOT/login";
		const API_USER_LOGOUT = "$API_USER_ROOT/logout";
		const API_USER_INFO = "$API_USER_ROOT/info";
		const API_USER_MOD = "$API_USER_ROOT/modify";
		const API_USER_DELETE = "$API_USER_ROOT/delete";
		const API_USER_ASK_RESET = "$API_USER_ROOT/askreset";
		const API_USER_RESET = "$API_USER_ROOT/reset";
	const API_USER_PING = "$API_ROOT/ping";

/* database */
const DATA_FOLDER = "./data";
const DB_LOCATION = "$DATA_FOLDER/db.sqlite3";
const DB_INIT_SQL = "$DATA_FOLDER/init.sql";
const DB_SESSION_COOKIES_INIT_SQL = "$DATA_FOLDER/sessionCookiesInit.sql";

/* form names */
const FORM_EMAIL = "email";
const FORM_PASSWD = "passwd";
const FORM_NEW_PASSWD = "new_passwd";
const FORM_NAME = "name";
const FORM_SURNAME = "surname";
const FORM_REMEMBER = "remember_me";
const QUERY_RTOKEN = "rtoken";

/* other */
const SESSION_COOKIE = "session";
const AUTH_COOKIE = "auth";
const LOG_FILE = "callaneticaitalia.log";
