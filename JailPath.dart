import 'package:path/path.dart';

class JailPath {
	final String root;
	final Context _context;
	
	JailPath(this.root) : _context = new Context(current: root);

	String jail(String path) {
		final relPath = _context.normalize(path.trim());
		return _join(root, relPath);
	}

	String jailUri({Uri uri, String uri_str}) =>
		jail(Uri.decodeComponent(uri_str ?? uri.path));
	
	
	String _join(String path1, String path2) {
		final int n = (path1[path1.length-1] == _context.separator ? 1 : 0) + (path2[0] == _context.separator ? 1 : 0);
		if(n == 2) return path1 + path2.substring(1);
		if(n == 1) return path1 + path2;
		return path1 + _context.separator + path2;
	}
}