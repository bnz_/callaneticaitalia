import 'dart:io';
import 'package:crypto/crypto.dart';
import 'error_page.dart';
import 'constants.dart';
import 'utils.dart';
import 'JailPath.dart';

/// `JailPath` of the assets
final sheriff = new JailPath(SITE_ROOT);
/// `Map` of the eTags (md5) of the assets
Map<String, String> eTags = {/* path of the asset : eTag in hex */};
/// content types for assets
final Map<String, ContentType> ct = {
	'html' : ContentType.html,
	'css' : ContentType.parse('text/css'),
	'js' : ContentType.parse('text/javascript'),
	'svg' : ContentType.parse('image/svg+xml'),
};


void handleAssets(String route, HttpRequest req) async {
	File asset = File(sheriff.jailUri(uri_str: route));
	dbg("route = ${route}, assets path = ${asset.path}");

	if(await asset.exists()) {
		try {
			req.response.headers.contentType = ct[route.substring(route.lastIndexOf('.')+1)] ?? req.headers.contentType;
			await req.response.addStream(asset.openRead());
			await req.response.close();
		} catch (e) {
			errorPage(500, req);
			log("Couldn't read form '${asset.path}': $e", LogLvl.error, req.connectionInfo);
		}
	} else {
		errorPage(404, req);
	}
}

/// calculates all the eTags of the assets
void calcETags() async {
	for(var file in Directory(sheriff.root).listSync(recursive: true)) {
		if(file is File)
			await saveETag(file);
	}
}

/// save the eTag (md5) of the file `f` inside the `eTags` maps
void saveETag(File f) async {
	eTags['/'+sheriff.jail(f.path)] = (await md5.bind(f.openRead()).first).toString();
}
