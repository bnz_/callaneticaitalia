FROM google/dart-runtime

# What google/dart-runtime already did:
# pug get
# copy all file from . to /app
# set /app as workdir

# compile sqlite3.so
RUN apt-get update && apt-get -y install gcc
RUN cd resources/sqlite && \
	gcc -O2 -c -fpic sqlite3.c && \
	gcc -shared -o libsqlite3.so sqlite3.o && \
	cp libsqlite3.so /app/data/sqlite3.so

# enable the production mode
ENV CALLANETICA_PRODUCTION=1

# TODO: volume of data (and for logs)

ENTRYPOINT ["/usr/bin/dart", "server.dart"]
