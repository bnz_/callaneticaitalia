
PRAGMA foreign_keys = ON;
PRAGMA synchronous = NORMAL;
PRAGMA temp_store = MEMORY;

-------------- User tables --------------

CREATE TABLE IF NOT EXISTS Users (
	email TEXT PRIMARY KEY,
	passwd BLOB NOT NULL, --Hash
	subscribed NUMERIC DEFAULT NULL, -- the date of subscription, if NULL it means that the user isn't subscribed
	name TEXT,
	surname TEXT,
	admin INTEGER DEFAULT 0 -- 1 = true
);

CREATE TABLE IF NOT EXISTS AuthCookies (
	selector BLOB PRIMARY KEY,
	token BLOB NOT NULL, --Hash
	user TEXT NOT NULL,
	expiration NUMERIC,
	FOREIGN KEY(user) REFERENCES Users(email) ON DELETE CASCADE
);


CREATE TABLE IF NOT EXISTS ResetTokens (
	token BLOB NOT NULL PRIMARY KEY, --Hash
	user TEXT NOT NULL,
	creation NUMBER NOT NULL,
	FOREIGN KEY(user) REFERENCES Users(email) ON DELETE CASCADE
);

-------------- Site data tables --------------

CREATE TABLE IF NOT EXISTS Category (
	name TEXT PRIMARY KEY
);

CREATE TABLE IF NOT EXISTS Videos (
	link TEXT PRIMARY KEY, --link to the video on mux
	-- private ?
	title TEXT,
	description TEXT,
	category TEXT, -- if null means that the video doesn't appear in the site
	FOREIGN KEY(category) REFERENCES Category(name) ON DELETE SET NULL
);


-- TODO: Site table, where to put various data relative to the site (home page title, texts, etc...)
-- (evaluate if a json is a better idea)

-- TODO: Notification table, where store email templates and other relative to the notifications via email


-------------- Admins --------------

-- UPDATE Users SET admin = 1 WHERE email = "<EMAIL>";


