-- table initialization for in-memory database of session cookies

CREATE TABLE SessionCookies (
	value TEXT PRIMARY KEY, -- it contains ONLY the value, not the entire cookie send in http header
	user TEXT NOT NULL,
	creation NUMBER DEFAULT CURRENT_TIMESTAMP
);
